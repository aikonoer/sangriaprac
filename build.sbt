import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.example",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "sangria",
    libraryDependencies += scalaTest % Test,
    libraryDependencies += "org.sangria-graphql" %% "sangria" % "1.4.1",
    libraryDependencies += "org.sangria-graphql" %% "sangria-circe" % "1.2.1"
)
