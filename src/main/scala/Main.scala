import io.circe.Json
import sangria.execution.Executor
import sangria.marshalling.circe._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future, duration}


object Main extends App {
  import sangria.macros._

  val query =
    graphql"""
      query Users {
        user(id: 0){
          firstName
          tweets {
            message
          }
        }

        users {
          firstName
          lastName
          tweets {
            message
          }
        }
      }
  """

  val result: Future[Json] = Executor.execute(SchemaDef.schema, query, new Repository)
  Await.result(result, Duration(5, duration.SECONDS))
  result.onComplete(println)
}
