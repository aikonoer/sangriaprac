case class User(id: Long, firstName: String, lastName: String, age: Option[Int])
case class Tweet(id: Long, userId: Long, message: String)

