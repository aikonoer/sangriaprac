class Repository {
  import Repository._

  def getUser(id: Long): Option[User] = users.find(_.id == id)
  def getAllUsers: List[User] = users
  def getTweetsByUser(id: Long): List[Tweet] = tweets.filter(_.userId == id)
  def getAllTweets: List[Tweet] = tweets
  def getTweet(id: Long): Option[Tweet] = tweets.find(_.id == id)
}

object Repository {
  val tweets = List(
    Tweet(0, 0, "Zero"),
    Tweet(1, 0, "One"),
    Tweet(2, 0, "Two"),
    Tweet(3, 0, "Three"),
    Tweet(4, 1, "Four"),
    Tweet(5, 1, "Five"),
    Tweet(6, 1, "Six"),
    Tweet(7, 1, "Seven")
  )

  val users = List(
    User(0, "Marcus", "Momongan", Some(2)),
    User(1, "Diane", "Momongan", None)
  )
}
