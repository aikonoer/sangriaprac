import sangria.schema._

object SchemaDef {

  val TweetType = ObjectType(
    "tweet",
    "short message",
    fields[Repository, Tweet](
      Field("message", StringType, resolve = _.value.message)
    )
  )

  val UserType = ObjectType(
    "user",
    "user details",
    fields[Repository, User](
      Field("firstName", StringType, resolve = _.value.firstName),
      Field("lastName", StringType, resolve = _.value.lastName),
      Field("age", OptionType(IntType), resolve = _.value.age),
      Field("tweets", ListType(TweetType),
        resolve = c => c.ctx.getTweetsByUser(c.value.id))
    )
  )

  val Id = Argument("id", LongType)

  val QueryType = ObjectType(
    "query",
    fields[Repository, Unit](
      Field("users", ListType(UserType), resolve = _.ctx.getAllUsers),
      Field("tweets", ListType(TweetType), resolve = _.ctx.getAllTweets),
      Field("user",
        OptionType(UserType),
        arguments = Id :: Nil,
        resolve = c => c.ctx.getUser(c.arg(Id))),
      Field("tweet",
        OptionType(TweetType),
        arguments = Id :: Nil,
        resolve = c => c.ctx.getTweet(c.arg(Id))
      )
    )
  )

  val schema = Schema(QueryType)

}
